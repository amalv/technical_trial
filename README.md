Prueba de habilidad
========================

1) Instalación del proyecto
---------------------------

    a) Composer:
        URL de referencia: https://getcomposer.org/download/
        
        Pasos a seguir para instalar composer en global:

        $ curl -sS https://getcomposer.org/installer | php
        $ mv composer.phar /usr/local/bin/composer

    b) Instalación de symfony:
        URL de referencia: http://symfony.com/doc/current/book/installation.html 

        El proyecto se encuentra bajo /var/www/prueba.

        Cómo dar permisos a la carpeta cache y logs:

        $ APACHEUSER=`ps aux | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data' | grep -v root | head -1 | cut -d\  -f1`
        $ sudo setfacl -R -m u:"$APACHEUSER":rwX -m u:`whoami`:rwX app/cache app/logs
        $ sudo setfacl -dR -m u:"$APACHEUSER":rwX -m u:`whoami`:rwX app/cache app/logs

    c) Comprobar que la instalación se ha realizado correctamente:
        http://localhost/prueba/web/config.php

    d) Acceder al controlador de desarrollo del proyecto para poder ver el formulario:
        http://localhost/prueba/web/app_dev.php

2) Inserción del formulario en la base de datos
-----------------------------------------------

Al acceder a la página principal se muestra un formulario. Hay que crear la programación necesaria para guardar
dicha información en la tabla "Contacto"

3) Validar la información a guardar
-----------------------------------
Los datos que son obligatorios son el name, el email, y el message.
Ademas, los datos como el email y location deben tener un formato valido. El formato válido para location,
es el formato de una direccion "calle, numero, ciudad"

4) Obtener la latitud y la longitud de las direcciones
-----------------------------------------------------

En caso de que introduzca la direccion consultar con el siguiente servicio

    $address = $this->container->get('bazinga_geocoder.geocoder')->geocode($addressString);

Actualizar el modelo para guardar la latitud y la longitud.

4) Crear un comando que muestre el teléfono y el email de los usuarios que no tengan información de latitud y longitud.

Debe aceptar un parametro para indicar el límite de resultados preestablecido a 10 elementos.
Para cada elemento, debe mostrar el nombre, el telefono y preguntar por una nueva dirección para que la que
en caso de introducir se buscará la latitud y la longitud mostrándola al usuario y actualizando el registro.

Al final del proceso se debe mostrar el numero de registros actualizados.

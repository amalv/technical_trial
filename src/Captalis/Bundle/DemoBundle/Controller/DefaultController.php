<?php

namespace Captalis\Bundle\DemoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Captalis\Bundle\DemoBundle\Form\ContactType;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        $formBuilder = $this->createForm(new ContactType());
        return $this->render('CaptalisDemoBundle:Default:index.html.twig', array('form' => $formBuilder->createView()));
    }
}

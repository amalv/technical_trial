<?php

namespace Captalis\Bundle\DemoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contact
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Contact
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=150, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text", nullable=true)
     */
    private $message;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="submited", type="datetime", nullable=true)
     */
    private $submited;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=125, nullable=true)
     */
    private $location;

    /**
     * @var string
     *
     * @ORM\Column(name="referal", type="string", length=255, nullable=true)
     */
    private $referal;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Contact
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Contact
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return Contact
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set submited
     *
     * @param \DateTime $submited
     * @return Contact
     */
    public function setSubmited($submited)
    {
        $this->submited = $submited;

        return $this;
    }

    /**
     * Get submited
     *
     * @return \DateTime 
     */
    public function getSubmited()
    {
        return $this->submited;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return Contact
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set referal
     *
     * @param string $referal
     * @return Contact
     */
    public function setReferal($referal)
    {
        $this->referal = $referal;

        return $this;
    }

    /**
     * Get referal
     *
     * @return string 
     */
    public function getReferal()
    {
        return $this->referal;
    }
}
